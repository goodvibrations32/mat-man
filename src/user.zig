const builtin = @import("builtin");
const Chameleon = @import("chameleon").Chameleon;
const std = @import("std");
const mem = @import("mem");

const platform_eol: u8 = if (builtin.os.tag == .linux) '\n' else '\r';

fn cmd_reader(Gred: std.fs.File.Reader) anyerror![]const u8 {

    var buf: [10]u8 = undefined;
    if (try Gred.readUntilDelimiterOrEof(buf[0..], platform_eol)) |inp| {
        std.debug.assert(inp.len <= buf.len);
        return inp;
    } else {
        // std.debug.print("this was not a float", .{inp});
        return error.anyerror;
    }
}

/// Ask for a dimention of the damper tile that is needed. (the thickness for now)
pub fn ask_dimention(message: std.fs.File.Writer, user_dimention: std.fs.File.Reader) anyerror!f64 {

    try message.print("\nenter dimention in meters: ", .{});

    return try std.fmt.parseFloat(f64, try cmd_reader(user_dimention));
}

/// Confirmation of choise for the damper material.
pub fn ask_confirm(usr_choise: anyerror![]const u8, message: std.fs.File.Writer, confirm: std.fs.File.Reader) !bool {
    comptime var cham = Chameleon.init(.Auto);

    // var buf: [10]u8 = undefined;
    std.log.warn(cham.yellowBright().bold().fmt("\nYou selected {!s}"), .{usr_choise});
    try message.print(cham.yellowBright().bold().fmt("Are you sure you want '{!s}' as your material of choise?? [y/n]: "), .{usr_choise});

    const answer: []const u8 = try cmd_reader(confirm);

    if (std.mem.eql(u8, answer, "y")) {
        return true;}
    else if (std.mem.eql(u8, answer, "n")) {
        return false;}
    else {return error.anyerror;}
}

/// Ask the user to give a material for the damper tile.
/// You can see exactly the name in projekt/src/materials.zig
///     For now  only 2 are implemented..:
///     (more info in materials.zig)
///     1. Tpu (TXXX)
///     2. Natural Rubber 
pub fn ask_material(inp_prompt: std.fs.File.Writer, usr_mat: std.fs.File.Reader) ![]const u8 {

    try inp_prompt.print("\nenter the material type: ", .{});

    return cmd_reader(usr_mat);

}
