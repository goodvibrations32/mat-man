const std = @import("std");
const log = @import("std.log");
const assert = std.debug.assert;
const testing = std.testing;
const default = @import("tile.zig");
const user = @import("user.zig");

const tile = default.Unit{.x_dim=2, .y_dim=2, .z_dim=2};

test "2d area of default rectangle" {
    const cross_section = tile.calc_2D(.m2);
    // this is because we have Rectangle tiles
    assert(try cross_section == std.math.pow(f64, tile.x_dim, 2));

    const unit = default.Unit{
        .x_dim = 1,
        .y_dim = 1,
        .z_dim = 1
    };
    // if the tile (x,y) == (1,1) then A = 1 m^2
    // std.debug.print("mm^2 when unit =(1,1,1) {}", .{unit_2d});
    assert(try unit.calc_2D(.mm2) == 1e5);
    assert(try unit.calc_2D(.cm2) == 1e4);
    assert(try unit.calc_2D(.m2) == 1);
}

test "net TPU area" {
    assert(try tile.net_area_peripheral(&default.Unit{.x_dim=2, .y_dim=2, .z_dim=2})
               == 0);
    assert(try tile.net_area_peripheral(&default.Unit{.x_dim=5, .y_dim=5, .z_dim=5})
               < 0);
}

test "volume calculation" {
    assert(try tile.volume(.m3) == std.math.pow(f64, tile.x_dim, 3));
    // std.log.debug("{:}", .{std.math.pow(f64, tile.x_dim, 6)});
    // assert(try tile.volume(.cm3) == std.math.pow(f64, tile.x_dim, 6));
    // assert(try tile.volume(.mm3) == std.math.pow(f64, tile.x_dim, 9));
}

test "asking damper material" {
    return error.SkipZigTest;
    //
    // TODO
    //
    // const material = try user.ask_material();
    // assert(std.mem.eql(u8, material, "tpu") or std.mem.eql(u8, material, "rubber"));
    // // assert(std.mem.eql(u8, material, "this should not be true for any reason"));
    // assert(material.len != 0);
}
