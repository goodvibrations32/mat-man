const std = @import("std");
const Chameleon = @import("chameleon").Chameleon;

const Mat = @import("materials.zig");
const tile = @import("tile.zig");
const user = @import("user.zig");

const PlexiGlass = Mat.PMMA;
const Tpu = Mat.T980M;
const NatRub = Mat.NatRub;

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const stdin = std.io.getStdOut().reader();
    comptime var cham = Chameleon.init(.Auto);
    _ = try stdout.print(cham.cyanBright().fmt(
        \\
        \\~~~~~~~~~~~~~~~~~~ Some defaults ....welcome.... ~~~~~~~~~~~~~~~~~
        \\
        \\ we are handling input in meters.. this means that 2 cm == 0.02
        \\ as input for the program. This is intentional and will propably
        \\ remain like that...
        \\
        \\ That is not to say that the output is also in meters, kg etc.
        \\
        // \\ Intentionaly the weight of each tile (damping or plexiglass for
        // \\ the structural ) is produced in gramms so the program favors the
        // \\ damping materials than the structural.
        // \\ for now the numbers refere to a single tile with known
        // \\ dimentions.
        \\~~~~~~~~~~~~~~~~~~      Default dimentions     ~~~~~~~~~~~~~~~~~~
        \\
        \\ The plexiglass tile has the bellow dimentions.
        \\ a rectangle face of 30cm x 30cm with a height of 2cm (20mm).
        \\
        \\ The damping material is chosen by the user.(almost..)
        \\ For now only Tpu and Natural Rubber are included in my tiny db
        \\ of 3 materials... (PMMA, T980M, Natural Rubber).
        \\
        \\~~~~~~~~~~~~~~~~~~~~ Confirmation user input ~~~~~~~~~~~~~~~~~~~~
        \\
        \\ For now the way to confirm a choise is implemented only on the
        \\ material type that will be used to calculate the approximate
        \\ weight of the damper tile. You will be prompt with the classic
        \\ [y/n] and you should respond with "y" or "n" lowercase without
        \\ spaces and *then* hit enter.
        \\
        \\~~~~~~~~~~~~~~~~~~~~ But is this usufull ??? ~~~~~~~~~~~~~~~~~~~~
        \\
        \\ Not at its current state... But it is a glimpse on how internal
        \\ tooling software and/or hardware can help in fast iteration and
        \\ prototyping but also preventing from future mistakes
        \\ (or create more... ヽ( ~～~ )ノ)
        \\
        // \\~~~~~~~~~~~~~~~~~  Pls try to crash the program  ~~~~~~~~~~~~~~~~
        // \\
        // \\ At points you are asked to input certain strings... See what
        // \\ can happen if you dont!!
        \\
    ), .{});

    // dont ask any more for the solid tile!!
    // we know what it is (30x30x2 [cm])
    const pmma_tile = &tile.Unit{ .x_dim = 0.3, .y_dim = 0.3, .z_dim = 0.02 };

    _ = try stdout.print(cham.redBright().bold().fmt("Enter height for the damper. (Peripheral damper pad)"), .{});

    const dead_area = tile.Unit{
        .x_dim = 0.2,
        .y_dim = 0.2,
        // .z_dim = 0.002  //dbg
        .z_dim = try user.ask_dimention(stdout, stdin),
    };

    // Calculate the damper rectangle that will be used for each side
    var DamperTile = &tile.Unit{.x_dim = pmma_tile.x_dim,
                                .y_dim = ((pmma_tile.y_dim - dead_area.y_dim) / 2.0),
                                .z_dim = dead_area.z_dim};

    try stdout.print(cham.redBright().bold().fmt(
        \\
        \\Enter the type of the material for the damping tile.
        \\      ["tpu": Tpu / "rubber": Natural Rubber]
        \\
    ), .{});

    var damp_material = try user.ask_material(stdout, stdin);
    std.debug.print("{s}\n", .{damp_material});
    const mat_choise = try user.ask_confirm(damp_material, stdout, stdin);

    if (!mat_choise) {
    damp_material = try user.ask_material(stdout, stdin);
    }

    // var pointer to constant value to avoid copy
    var damper_material_of_choise: Mat.MaterialProps = undefined;

    if (damp_material.len > 0) {
        // std.debug.print("array length {d}", .{damp_material.len});
        if (std.mem.eql(u8, damp_material, "tpu")) {
            damper_material_of_choise = Tpu;
        } else if (std.mem.eql(u8, damp_material, "rubber")) {
            damper_material_of_choise = NatRub;
        // } else if (!std.mem.eql(u8, damp_material, "rubber") or !std.mem.eql(u8, damp_material, "tpu")) {

        //     std.log.err(cham.redBright().bold().fmt(
        //         "This material is not yet implemented... please try either tpu or rubber.."), .{});
        //     damp_material = try user.ask_material(stdout, stdin);

        //     if (std.mem.eql(u8, damp_material, "tpu")) {
        //         damper_material_of_choise = Tpu;}
        //     else if (std.mem.eql(u8, damp_material, "rubber")) {
        //         damper_material_of_choise = NatRub;}
        } else {
            // unreachable;
            damper_material_of_choise = undefined;
        }
    }

    const damper_tile_area = pmma_tile.net_area_peripheral(&dead_area);
    const area_2d = try pmma_tile.calc_2D(.m2);
    std.debug.print("2d areas (tile and damper): {!}, {!:.2}\n", .{ area_2d, damper_tile_area });

    const pmma_weight = try pmma_tile.volume(.cm3) * PlexiGlass.densityGcc;
    // we need to multiply because of how the damper tile dimentions are computed
    const damper_weight = try DamperTile.volume(.cm3) * (damper_material_of_choise.densityGcc * 4.0);
    try stdout.print("gramms of plexiglass / damper tile resp.: {d:.3} / {d:.3}\n", .{ pmma_weight, damper_weight });

    // std.debug.print("\ndamper data: = {?:.3}\n", .{DamperTile});
}
