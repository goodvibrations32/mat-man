/// A struct to store material properties we need for the analysis.
pub const MaterialProps = struct {
    /// Tensile yield strength in MPa.
    tensileMPaY: f64,
    /// Impact strength in MPa.
    impactStrength: f64,
    /// Material density in gramms/cm^3
    densityGcc: f64,
};

/// TPU properties from the net
pub const T980M: MaterialProps = .{.densityGcc = 1.21,
                                   .tensileMPaY = 27,
                                   .impactStrength = 3.2};
/// Plexiglass properties from ASCM
pub const PMMA: MaterialProps = .{.densityGcc = 1.18,
                                  .tensileMPaY = 55.15,
                                  .impactStrength = 3.2};
/// Natural rubber properties (MatWeb)
pub const NatRub: MaterialProps = .{.densityGcc = 0.95,
                                    .tensileMPaY = 28,
                                    .impactStrength = undefined};
// -----------------------------------
// other materials for experimenting
// -----------------------------------
// const T990M   : MaterialProps = .{};
// const T998M   : MaterialProps = .{};
// const T960DM  : MaterialProps = .{};
// const E980M   : MaterialProps = .{};
// const E990M   : MaterialProps = .{};
// const E998M   : MaterialProps = .{};
// const T995NC  : MaterialProps = .{};
// const T995NCS : MaterialProps = .{};
