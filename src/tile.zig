const std = @import("std");
const user = @import("user.zig");


pub const MeasUnits = enum {
    mm2,
    cm2,
    m2,
    mm3,
    cm3,
    m3,
    err,
};

pub const Unit: type = struct {
    x_dim: f64,
    y_dim: f64,
    z_dim: anyerror!f64,

    /// Calculate the 2 dimentional cross section
    ///     |Variables|
    ///         self: The 3 dimentional tile Unit(x, y, z)
    ///
    ///         measunit: The measurement unit that the result will be
    ///                   represented.
    pub fn calc_2D(self: Unit, measunit: MeasUnits) anyerror!f64 {
        switch (measunit) {
            .mm2 => return self.x_dim * self.y_dim * 1e5,
            .cm2 => return (self.x_dim * self.y_dim) * 1e4,
            .m2 => return self.x_dim * self.y_dim,
            else => return error.anyerror,
        }
    }

    /// Calculate the net area of a hollow tile by removing a second "imaginary"
    /// tile and using this for acting like the damper tile.
    ///
    /// TOD: Implement something for this !!!~~~!!!
    ///
    ///     |Variables|
    ///
    ///         self: The 3 dimentional tile Unit(x, y, z)
    ///
    ///         area2: The "discarded" tile to produce an X rectangle with base
    ///                the same as the orignal and height 10 cm.
    ///                example:  ___________    ___________
    ///                         |           |  |  _______  |
    ///                         |           |  | | empty | |
    ///                  height |           |  | | space | |
    ///                         |           |  | |_______| |
    ///                         |___________|  |___________| new height
    ///                             base         same base
    ///
    ///     |References|
    ///
    ///         https://www.paramvisions.com/2020/10/how-to-calculate-weight-of-hollow.html
    pub fn net_area_peripheral(self: Unit, area2: *const Unit) !f64 {
        const original_area = self.x_dim * self.y_dim;
        const empty_middle_area = area2.x_dim * area2.y_dim;

        return original_area - empty_middle_area;
        // const height = self.y_dim - area2.y_dim;
    }

    /// Calculate the volume of a 3D cubic tile
    ///     |Variables|
    ///         self: The 3 dimentional tile Unit(x, y, z)
    ///
    ///         measunit: The measurement unit that the result will be
    ///                   represented.
    pub fn volume(self: Unit, measunit: MeasUnits) !f64 {
        // self.z_dim catch |err| (std.debug.print("this is the error: {?}", .{err}));
        const my_z_axis_in_meters = (self.z_dim) catch |err| switch (err) {
            error.InvalidCharacter => err,
            else => self.z_dim,
        };
        switch (measunit) {
            .mm3 => return (self.x_dim * self.y_dim * try my_z_axis_in_meters) * 1e9,
            .cm3 => return (self.x_dim * self.y_dim * try my_z_axis_in_meters) * 1e6,
            .m3 => return self.x_dim * self.y_dim * try my_z_axis_in_meters,
            // .err => |err| std.debug.print("{?}", .{err}),
            else => return error.anyerror
        }
    }
};
