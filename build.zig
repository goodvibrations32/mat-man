const std = @import("std");

const test_targets = [_]std.Target.Query{
    .{}, // native
    // .{.cpu_arch = .x86_64, .os_tag = .linux,},
    // .{.cpu_arch = .aarch64, .os_tag = .linux,},
    // .{.cpu_arch = .x86_64, .os_tag = .windows,},

    // Automated testing seems to fail at these platforms... Don't know why...
    // .{.cpu_arch = .aarch64, .os_tag = .macos,},
    // .{.cpu_arch = .aarch64, .os_tag = .linux,},
};
// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) !void {
    const optimize = b.standardOptimizeOption(.{});
    _ = optimize;
    const cham = b.dependency("chameleon", .{});
    const test_step = b.step("test", "Run unit tests");
    const run_step = b.step("run", "Run the app");

    for (test_targets) |t| {
        const executable = b.addExecutable(.{
            .name = "tpu_props_safe",
            .root_source_file = b.path("src/main.zig"),
            .target = b.resolveTargetQuery(t),
            .optimize = .Debug,
        });

        executable.root_module.addImport("chameleon", cham.module("chameleon"));
        const targ = b.addInstallArtifact(executable, .{
            .dest_dir = .{
                .override = .{
                    .custom = try t.zigTriple(b.allocator),
                },
            },
        });

        b.getInstallStep().dependOn(&targ.step);

        const run_cmd = b.addRunArtifact(executable);

        run_cmd.step.dependOn(b.getInstallStep());

        if (b.args) |args| {
            run_cmd.addArgs(args);
        }

        run_step.dependOn(&run_cmd.step);

        // Creates a step for unit testing. This only builds the test executable
        // but does not run it.
        const unit_tests = b.addTest(.{
            .root_source_file = b.path("./src/tests.zig"),
            .target = b.resolveTargetQuery(t),
        });
        unit_tests.root_module.addImport("chameleon", cham.module("chameleon"));
        const run_unit_tests = b.addRunArtifact(unit_tests);
        // test_step.dependOn;
        // run_cmd.step.dependOn();
        test_step.dependOn(&run_unit_tests.step);

    // const cham_dep = b.dependency("chameleon", .{});
    // const cham_mod = cham_dep.module("chameleon");
    // exe_linux.addModule("chameleon", cham_mod);
    }
}
